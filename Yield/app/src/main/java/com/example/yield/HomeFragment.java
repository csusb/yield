package com.example.yield;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.yield.R;

/**
 * Class for HomeFragment Fragment that's in the Navigation Graph
 * HomeFragment -> ...
 */
public class HomeFragment extends Fragment {

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    /**
     * onViewCreated() gets called AFTER onCreateView() has returned.
     *
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // view.findViewById() returns the "Add Yield" button in fragment_home.xml
        // If that button is clicked, the user will be taken to the addYieldFragment Fragment
        view.findViewById(R.id.add_yield_button).setOnClickListener(Navigation.createNavigateOnClickListener(R.id.addYieldFragment, null));

        // view.findViewById() returns the "View Yield" button in fragment_home.xml
        // If that button is clicked, the user will be taken to the viewYieldFragment Fragment
        view.findViewById(R.id.view_yield_button).setOnClickListener(Navigation.createNavigateOnClickListener(R.id.viewYieldFragment, null));

        // view.findViewById() returns the "Settings" button in fragment_home.xml
        // If that button is clicked, the user will be taken to the settingsFragment Fragment
        view.findViewById(R.id.settings_button).setOnClickListener(Navigation.createNavigateOnClickListener(R.id.settingsFragment, null));
    }
}
