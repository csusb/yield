package com.example.yield;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Class for ManualEntryFragment Fragment that's in the Navigation Graph
 * HomeFragment -> AddYieldFragment -> ManualEntryOrImageEntryFragment -> ManualEntryFragment -> ...
 */
public class ManualEntryFragment extends Fragment {

    private String nameOfYield;
    private String firstOperator;
    private String secondOperator;
    private String spinnerChoice;
    private String countFromUser;

    // This is the action that takes the user from ManualEntryFragment -> FinalCalcOfYieldFragment
    private ManualEntryFragmentDirections.ManualEntryToFinal action;

    // This variable is created because view/getView() may be used in methods outside of onViewCreated()
    // and when view/getView() is used outside of onViewCreated(), it may be null, so I set mView to view
    // in onViewCreated()
    private View mView;

    public ManualEntryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manual_entry, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Created just in case view/getView() returns null when called outside of this method
        mView = view;

        // Method to get the values for the arguments from ManualEntryOrImageEntryFragment
        getDataFromPrevDestination();

        // view.findViewById() returns the "Continue" button in fragment_manual_entry.xml
        // If that button is clicked, the user will be taken to the FinalCalcOfYieldFragment Fragment
        //view.findViewById(R.id.manual_count_continue).setOnClickListener(Navigation.createNavigateOnClickListener(R.id.finalCalcOfYieldFragment, null));
        view.findViewById(R.id.manual_count_continue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDataToPass();
                nextDestination();
            }
        });
    }

    /**
     * getDataFromPrevDestination() stores the values of the arguments that were passed from
     * ManualEntryOrImageEntryFragment
     */
    private void getDataFromPrevDestination() {
        nameOfYield = ManualEntryOrImageEntryFragmentArgs.fromBundle(getArguments()).getNameOfYield();
        firstOperator = ManualEntryOrImageEntryFragmentArgs.fromBundle(getArguments()).getFirstOperator();
        secondOperator = ManualEntryOrImageEntryFragmentArgs.fromBundle(getArguments()).getSecondOperator();
        spinnerChoice = ManualEntryOrImageEntryFragmentArgs.fromBundle(getArguments()).getTypeOfCrop();

    }

    /**
     * setDataToPass() will get the manual count that the user entered
     * it will then set the arguments that were created in FinalCalcOfYieldFragment in the nav graph
     */
    private void setDataToPass() {
        EditText manualCount = (EditText) mView.findViewById(R.id.manual_count);

        countFromUser = manualCount.getText().toString();

        // This is the action that takes the user from ManualEntryFragment to FinalCalcOfYieldFragment
        // the method manualEntryToFinal() was created for us when we created the action to go from
        // ManualEntryFragment to FinalCalcOfYieldFragment and the parameters it takes are the arguments
        action = ManualEntryFragmentDirections.manualEntryToFinal(nameOfYield, firstOperator, secondOperator,
                        spinnerChoice, countFromUser);

        // setter functions created for us when we created the arguments in FinalCalcOfYieldFragment
        action.setNameOfYield(nameOfYield);
        action.setFirstOperator(firstOperator);
        action.setSecondOperator(secondOperator);
        action.setTypeOfCrop(spinnerChoice);
        action.setCount(countFromUser);


    }

    /**
     * nextDestination() simply checks if the arguments will be empty and notify's the user if
     * they didn't enter a number for the count
     * If the field isn't empty then the user will be navigated to FinalCalcOfYieldFragment
     */
    private void nextDestination() {
        if (countFromUser.isEmpty() || nameOfYield.isEmpty() || firstOperator.isEmpty() || secondOperator.isEmpty() || spinnerChoice.isEmpty()) {
            Toast.makeText(getContext(), "Please enter count", Toast.LENGTH_SHORT).show();
            System.out.println("one of the arguments are empty");
        } else {
            Navigation.findNavController(mView).navigate(action);
        }
    }
}
