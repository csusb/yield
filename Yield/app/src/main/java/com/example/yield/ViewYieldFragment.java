package com.example.yield;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.yield.R;

import java.util.List;


/**
 * Class for ViewYieldFragment Fragment that's in the Navigation Graph
 * HomeFragment -> ViewYieldFragment -> ...
 */
public class ViewYieldFragment extends Fragment {

    //TxtInfo is a new variable that references the TextView on the Fragment_view_yield
    private TextView TxtInfo;

    public ViewYieldFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_view_yield, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        //This will use list_of_yields from fragment_view_yield.xml
        TxtInfo = view.findViewById(R.id.list_of_yields);


        //Read info from database (list)
        List<User> users = MainActivity.myAppDatabase.myDao().getUser();

        String info = "";
        for (User usr : users) {
            //fetch info from database User
            //Database new_name : list
            int id = usr.getId();
            String NameYield = usr.getNameOfYield();
            String FOperator = usr.getFirstOperator();
            String SOperator = usr.getSecondOperator();
            String SpinChoice = usr.getSpinnerChoice();
            String count_u = usr.getCount();
            String yieldOut = usr.getYield();
            String valueOut = usr.getValue();


            //displays the following in the View Yield Screen with the data in the variables created above

            info = info + "\n\n"+"ID : "+id+"\nName Of Yield: " + NameYield + "\nChosen Crop: " + SpinChoice + "\n" + "Size of Field:  " + FOperator + "  meters  * " + "  " + SOperator +" meters"+ "\n" + "Count of Crop in Area: " + count_u + "\nEstimated yield: " + yieldOut + " grams\nEstimated minimum price: " + valueOut;
        }
        TxtInfo.setText(info);


    }
}
