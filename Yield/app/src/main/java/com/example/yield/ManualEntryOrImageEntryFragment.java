package com.example.yield;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.tensorflow.lite.examples.detection.DetectorActivity;


/**
 * Class for ManualEntryOrImageEntryFragment Fragment that's in the Navigation Graph
 * HomeFragment -> AddYieldFragment -> ManualEntryOrImageEntryFragment -> ...
 */
public class ManualEntryOrImageEntryFragment extends Fragment {


    public ManualEntryOrImageEntryFragment() {
        // Required empty public constructor
    }

    private String nameOfYield;
    private String firstOperator;
    private String secondOperator;
    private String spinnerChoice;

    // mView is created so that we can set the view to this variable in onViewCreated()
    // this is a workaround instead of calling getView() outside of onViewCreated() which
    //  can lead to a nullPointerException
    private View mView;

    // This action (in the nav graph) corresponds to the action that takes the user from
    // ManualEntryOrImageEntryFragment to ManualEntryFragment
    private ManualEntryOrImageEntryFragmentDirections.ToManualEntry actionToManualEntry;

    // This action (in the nav graph) corresponds to the action that takes the user from
    // ManualEntryOrImageentryFragment to ManualEntryFragment
    private ManualEntryOrImageEntryFragmentDirections.ObjectDetectionChosenAction actionToObjDetChosen;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_manual_entry_or_image_entry, container, false);
    }
    /**
     * onViewCreated() gets called AFTER onCreateView() has returned.
     * This is where the buttons are connected and the logic happens inside those said buttons
     *
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // view/getView() gets called in methods outside of onViewCreated() and when it does get called
        // it may be null, so this is a workaround
        mView = view;

        // gets the data from AddYieldFragment
        getDataFromPrevDestination();

        // view.findViewById() returns the "Manual Entry" button in fragment_manual_entry_or_image.xml
        // If that button is clicked, the user will be taken to the ManualEntryFragment Fragment
        // along with the data from the previous destination
        view.findViewById(R.id.manual_entry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDataToPass(1);
                nextDestination(1);
            }
        });

        // view.findViewById() returns the "Object Detection" button in fragment_manual_entry_or_image.xml
        // If that button is clicked, the user will be taken to the ObjectDetectionChosenFragment fragment
        // along with the data from the previous destination
        view.findViewById(R.id.object_detection_chosen_action).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               setDataToPass(2);
               nextDestination(2);
            }
        });
    }

    /**
     * getDataFromPrevDestination() retrieves the Arguments that were passed from AddYieldFragment
     * Those values are stored in strings to be used later
     */
    private void getDataFromPrevDestination() {

        // These four strings are given the value of the argument that was sent and set from the AddYieldFragment
        // ManualEntryOrImageEntryFragmentArgs was created for us when we created Arguments to be passed from
        //  destination to destination
        // The getter functions were created by Android Studio when we created the Arguments in the nav graph
        nameOfYield = ManualEntryOrImageEntryFragmentArgs.fromBundle(getArguments()).getNameOfYield();
        firstOperator = ManualEntryOrImageEntryFragmentArgs.fromBundle(getArguments()).getFirstOperator();
        secondOperator = ManualEntryOrImageEntryFragmentArgs.fromBundle(getArguments()).getSecondOperator();
        spinnerChoice = ManualEntryOrImageEntryFragmentArgs.fromBundle(getArguments()).getTypeOfCrop();

    }

    /**
     * setDataToPass() takes the data retrieved from getDataFromPrevDestination() and will pass
     * it to ManualEntryFragment or ObjectDetectionChosenFragment based upon the parameter passed
     */
    private void setDataToPass(int value) {

        // This is the action that takes the user from ManualEntryOrImageEntryFragment to ManualEntryFragment
        // the function toManualEntry() was created for that specific action
        // the parameters for toManualEntry() are the values that will be sent to ManualEntryFragment

        switch (value) {
            case 1:
                actionToManualEntry = ManualEntryOrImageEntryFragmentDirections.toManualEntry(nameOfYield, firstOperator, secondOperator, spinnerChoice);

                // These setter functions were created when we created the arguments in ManualEntryFragment in the nav graph
                // The values were set in getDataFromPrevDestination()
                actionToManualEntry.setNameOfYield(nameOfYield);
                actionToManualEntry.setFirstOperator(firstOperator);
                actionToManualEntry.setSecondOperator(secondOperator);
                actionToManualEntry.setTypeOfCrop(spinnerChoice);
                break;
            case 2:
                actionToObjDetChosen = ManualEntryOrImageEntryFragmentDirections.objectDetectionChosenAction(firstOperator, secondOperator, nameOfYield, spinnerChoice);

                // These setter functions were created when we created the arguments in ObjectDetectionChosenFragment in the nav graph
                // The values were set in getDataFromPrevDestination()
                actionToObjDetChosen.setNameOfYield(nameOfYield);
                actionToObjDetChosen.setFirstOperator(firstOperator);
                actionToObjDetChosen.setSecondOperator(secondOperator);
                actionToObjDetChosen.setTypeOfCrop(spinnerChoice);
                break;
            default:
                System.out.println("default switch");
                break;

        }
    }

    /**
     * nextDestination() simply checks if any of the values for arguments is empty
     * If not, the user will be sent to ManualEntryFragment or ObjectDetectionChosenFragment
     * based on parameter passed above
     */
    private void nextDestination(int value) {
        // Simple check to see if any of the values are empty
        // If they're not empty, the user will be navigated to ManualEntryFragment or ObjectDetectionChosenFragment
        if (nameOfYield.isEmpty() || firstOperator.isEmpty() || secondOperator.isEmpty() || spinnerChoice.isEmpty()) {
            System.out.println("Error");
        } else {
            switch (value) {
                case 1:
                    Navigation.findNavController(mView).navigate(actionToManualEntry);
                    break;
                case 2:
                    Navigation.findNavController(mView).navigate(actionToObjDetChosen);
                    break;
                default:
                    break;

            }
        }
    }

}
