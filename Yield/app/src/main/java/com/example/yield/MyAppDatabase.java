package com.example.yield;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {User.class},version = 1)
public abstract class MyAppDatabase extends RoomDatabase
{
    //MyDao represents the My Data Access Object
    //reference Room database for more information on the database
    //create an abstract variable that will return
    public abstract MyDao myDao();


}
