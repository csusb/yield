package com.example.yield;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface MyDao
{
    //my database access object
    //reference Room database and @Dao for more information on the database


    //below does insertion into database used in FinalCalcOfYield.java
    //Data from AddYieldFragment and ManualEntryFragment.java is passed onto FinalCalcOfYield
    //where it is then saved to the database
    @Insert
    public void addUser(User user);


    //This function will list user info from database. Used in ViewYieldFragment.java
    @Query("select * from users")
    public List<User> getUser();
}
