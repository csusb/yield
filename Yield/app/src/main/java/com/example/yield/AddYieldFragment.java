package com.example.yield;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.yield.R;

/**
 * Class for AddYieldFragment Fragment that's in the Navigation Graph
 * HomeFragment -> AddYieldFragment -> ...
 */
public class AddYieldFragment extends Fragment {

    private Spinner spinner;
    private String name;
    private String spinnerChoice;
    private String first;
    private String second;

    /* mView and mContext are created to be used in other methods
        instead of having to call getView() or getContext() since they both could be null
        when called inside other methods other than onViewCreated().
        The values are set in onViewCreated().
    */
    private View mView;
    private Context mContext;

    // This action (in the nav graph) corresponds to the action that takes the user from
    // AddYieldFragment to ManualEntryOrImageEntryFragment
    private AddYieldFragmentDirections.AddInputToImage action ;


    // Required empty public constructor
    public AddYieldFragment() { }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflates the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_yield, container, false);
    }

    /**
     * onViewCreated() gets called AFTER onCreateView() has returned.
     *
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // view/getView() and getContext() are called in other methods outside onViewCreated()
        // this is a workaround to the possible chance that view/getView() or getContext() are null
        mView = view;
        mContext = getContext();

        // setSpinner() gets called to create the Spinner (dropdown menu) that will hold
        //  the string values for our crop
        setSpinner();

        /*
         * view.findViewById() returns the button in fragment_add_yield.xml
         *
         * If the user clicks on that button, createNavigateOnClickListener() is called
         * and the user will be taken to the manualEntryOrImageFragment Fragment in the
         * navigation graph
         * The values entered by the user will get saved and passed to the next destination
         */
        view.findViewById(R.id.go_to_manual_or_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataToPass();
                setDataToPass();
                nextDestination();
            }
        });
    }

    /**
     * setSpinner() gets called in onViewCreated to set up the Spinner (dropdown menu)
     * that holds the values for our crops
     */
    private void setSpinner() {

        //id: crop_spinner is created in fragment_add_yield.xml
        spinner = mView.findViewById(R.id.crop_spinner);

        // Create an ArrayAdapter using the string array, crops_array, which is found in res/values/strings.xml
        // and gives a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(mContext,
                R.array.crops_array, android.R.layout.simple_spinner_item);

        // Specifies the layout to use when the list of choices appear
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Applies the adapter to the spinner
        spinner.setAdapter(adapter);

    }

    /**
     * getDataToPass() gets called in onViewCreated(), specifically in the onClick() for our button
     * with android:id = go_to_manual_or_image.
     * This method just gets the values entered by the user and stores them in strings for later use
     */
    private void getDataToPass() {

        // nameOfYield is the name the user chooses to name the yield
        EditText nameOfYield = mView.findViewById(R.id.edit_name_of_yield);

        // firstOperator is the first number in the Y x Y formula for the area they want to purchase
        EditText firstOperator = mView.findViewById(R.id.edit_first_size_of_yield);

        // secondOperator is the second number in the Y x Y formula for the area they want to purchase
        EditText secondOperator = mView.findViewById(R.id.edit_second_size_of_yield);

        // These four are getting the values from the EditText's or the Spinner
        name = nameOfYield.getText().toString();
        spinnerChoice = spinner.getSelectedItem().toString();
        first = firstOperator.getText().toString();
        second = secondOperator.getText().toString();

    }

    /**
     * setDataToPass() gets called in onViewCreated(), specifically in the onClick() for our button
     * with android:id = go_to_manual_or_image.
     * This method will create the action to go to the next destination and use the setter methods, that
     * were created when we created the Arguments for the receiving destination, to set the values
     * of the Arguments to our strings values in getDataToPass()
     */
    private void setDataToPass() {

        // This is the action that takes the user from AddYieldFragment to ManualEntryOrImageEntryFragment
        // along with the parameters that are needed to send the data (which are 4 strings)
        action = AddYieldFragmentDirections.addInputToImage(name,
                spinnerChoice, first, second);

        // These set functions are created for us when we add "Arguments" to the receiving destination
        // Each "Argument" will be set with the strings created in getDataToPass()
        action.setNameOfYield(name);
        action.setTypeOfCrop(spinnerChoice);
        action.setFirstOperator(first);
        action.setSecondOperator(second);

    }

    /**
     * nextDestination() gets called in onViewCreated(), specifically in the onClick() for our button
     * with android:id = go_to_manual_or_image.
     * This method was created to add a little more abstraction. It checks to see if the values gathered
     * from the user are non-null. If so, then a pop-up will notify the user, else it will go to the next
     * destination.
     */
    private void nextDestination() {

        // Since we don't want to go to the next screen with the fields being empty,
        // a simple check to see if the values of the strings from getDataToPass() are empty
        // If they are empty, then a Toast will pop-up telling the user that they must enter for all fields
        // Else we will go to the next screen taking the action from AddYieldFragment to ManualEntryOrImageFragment
        if (name.isEmpty() || first.isEmpty() || second.isEmpty() || spinnerChoice.isEmpty()) {
            Toast.makeText(getContext(), "Please enter information for all fields", Toast.LENGTH_SHORT).show();
        } else {
            Navigation.findNavController(mView).navigate(action);
        }
    }

}
