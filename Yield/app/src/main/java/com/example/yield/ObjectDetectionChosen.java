package com.example.yield;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.tensorflow.lite.examples.detection.DetectorActivity;

import static android.app.Activity.RESULT_OK;


/**
 * Class for ObjectDetectionChosen Fragment that's in the navigation graph
 * HomeFragment -> AddYieldFragment -> ManualEntryOrImageEntryFragment -> ObjectDetectionChosen -> ...
 */
public class ObjectDetectionChosen extends Fragment {

    private TextView countFromTFLiteTV;
    private int countFromTFLite;
    //countToString set to 0 since "count" from SafeArgs is non-null
    private String countToString = "0";
    private String nameOfYield;
    private String firstOperator;
    private String secondOperator;
    private String spinnerChoice;

    private View mView;

    // This is the action created in the nav_graph.xml that takes the user from
    // ObjectDetectionChosenFragment -> FinalCalcOfYieldFragment
    private ObjectDetectionChosenDirections.ObjDetToCalc action;

    public ObjectDetectionChosen() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_object_detection_chosen, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // mView set to view since we need to call view outside of onViewCreated()
        mView = view;

        // TextView that will display the count returned from TFLite
        // Set to "0" as starting
        countFromTFLiteTV = view.findViewById(R.id.count_from_tf_lite);
        countFromTFLiteTV.setText("0");

        // gets the data that was passed from ManualEntryOrImageEntryFragment
        getDataFromPrevDestination();

        // onClickListener set for the "Launch TFLite" button
        view.findViewById(R.id.launch_tf_lite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // If this button is clicked, an intent is created to call DetectorActivity
                // which is the activity in our tflite module
                // startActivityForResult() is used so we can get data from DetectorActivity once returned
                Intent intent = new Intent(getContext(), DetectorActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        // onClickListener set for "Continue" button
        view.findViewById(R.id.go_to_calc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //sets the data to be passed to FinalCalcOfYieldFragment
                setDataToPass();
                //uses the proper action to go to the next destination
                nextDestination();
            }
        });

    }

    /**
     *
     * @param requestCode
     * @param resultCode
     * @param data
     *
     * onActivityResult() is created so we can get the data that was passed from DetectorActivity
     * the data that gets passed back is the current number of objects it detects
     * once the user returns (back/up button) to ObjectDetectionChosenFragment
     * that value that was passed back will be passed to countFromTFLite and the TextView will
     * display the value from countFromTFLite
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                countFromTFLite = data.getIntExtra("countFromTFLite", 0);
                countToString = Integer.toString(countFromTFLite);
                countFromTFLiteTV.setText(countToString);
            }
        }
    }

    /**
     * getDataFromPrevDestination() is called when the onViewCreated() function is called
     * It just stores the values passed from the previous fragment to values set in this fragment
     */
    private void getDataFromPrevDestination() {
        nameOfYield = ManualEntryOrImageEntryFragmentArgs.fromBundle(getArguments()).getNameOfYield();
        firstOperator = ManualEntryOrImageEntryFragmentArgs.fromBundle(getArguments()).getFirstOperator();
        secondOperator = ManualEntryOrImageEntryFragmentArgs.fromBundle(getArguments()).getSecondOperator();
        spinnerChoice = ManualEntryOrImageEntryFragmentArgs.fromBundle(getArguments()).getTypeOfCrop();
    }

    /**
     * setDataToPass() connects the proper action from ObjectDetectionChosenFragment to FinalCalcOfYieldFragment
     * then uses the pre-made setter functions to set the values for each argument from SafeArgs in the nav_graph
     */
    private void setDataToPass() {
        action = ObjectDetectionChosenDirections.objDetToCalc(nameOfYield, firstOperator, secondOperator, spinnerChoice, countToString);

        action.setNameOfYield(nameOfYield);
        action.setFirstOperator(firstOperator);
        action.setSecondOperator(secondOperator);
        action.setTypeOfCrop(spinnerChoice);
        action.setCount(countToString);
    }

    /**
     * nextDestination() checks if any of the values are empty, if so, a Toast will pop up,
     * if not, then the user will be taken to FinalCalcOfYieldFragment
     */
    private void nextDestination() {
        if (countToString.isEmpty() || nameOfYield.isEmpty() || firstOperator.isEmpty() || secondOperator.isEmpty() || spinnerChoice.isEmpty()) {
            Toast.makeText(getContext(), "Please launch TFLite to get count", Toast.LENGTH_SHORT).show();
        } else {
            Navigation.findNavController(mView).navigate(action);
        }
    }
}
