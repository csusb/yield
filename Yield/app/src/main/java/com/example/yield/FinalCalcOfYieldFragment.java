package com.example.yield;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * Class for FinalCalcOfYieldFragment Fragment that's in the navigation graph
 * HomeFragment -> AddYieldFragment -> ManualEntryOrImageEntryFragment -> ... -> FinalCalcOfYieldFragment -> ...
 */
public class FinalCalcOfYieldFragment extends Fragment {

    private String nameOfYield;
    private String firstOperator;
    private String secondOperator;
    private String spinnerChoice;
    private String count;
    private float estimatedYield;
    private double estimatedPrice;
    private double conversionFactor;
    private View mView;
    private String currency;
    private String currencySymbol;
    private String displayPrice;


    public FinalCalcOfYieldFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_final_calc_of_yield, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // created and set to view in onViewCreated() just in case we need to use view/getView()
        // outside of onViewCreated()
        mView = view;

        // Retrieves data that was set using the setter functions in ManualEntryFragment
        getDataFromPrevDestination();

        // Just sets the text of the TextView of id manual_count
        setCountPlaceholder();

        // Connects the "Calculate" button to see if it's clicked and will then calculate the yield
        // and set the appropriate TextView to the value from the calculation
        view.findViewById(R.id.calculate_count_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateEstimatedYield();
                setEstimatedYieldDisplay();

                final TextView symbolTextView = (TextView) view.findViewById(R.id.money_unit);
                symbolTextView.setText(currencySymbol);

            }
        });

        // Retrieves settings data from the cache.
        // Currency conversion rates retrieved on 6/10/19. Source: https://api.exchangeratesapi.io/latest?base=USD
        currency = readFromCache(getContext());
        if (currency.equals("USD")){
            currencySymbol = "$";
            conversionFactor = 1.0;
        } else if (currency.equals("EURO")){
            currencySymbol = "€";
            conversionFactor = 0.8848774445;
        } else if (currency.equals("YEN")){
            currencySymbol = "¥";
            conversionFactor = 108.6452526325;
        }

        //Add Yield Function
        view.findViewById(R.id.save_yield_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Function that save the yield using the User database (Room Database)
                saveYield();
            }
        });

    }

    /**
     * Stores the values that were set using the setter functions in ManualEntryFragment
     * and stores them in our private Strings declared for this class
     * These strings will be used later for calculations.
     */
    private void getDataFromPrevDestination() {
        nameOfYield = FinalCalcOfYieldFragmentArgs.fromBundle(getArguments()).getNameOfYield();
        firstOperator = FinalCalcOfYieldFragmentArgs.fromBundle(getArguments()).getFirstOperator();
        secondOperator = FinalCalcOfYieldFragmentArgs.fromBundle(getArguments()).getSecondOperator();
        spinnerChoice = FinalCalcOfYieldFragmentArgs.fromBundle(getArguments()).getTypeOfCrop();
        count = FinalCalcOfYieldFragmentArgs.fromBundle(getArguments()).getCount();

    }

    /**
     * Will set the text of our TextView for id manual_count to the value that was retrieved in
     * getDataFromPrevDestination()
     */
    private void setCountPlaceholder() {
        TextView countPlaceholder = mView.findViewById(R.id.manual_count);
        countPlaceholder.setText(count);
    }

    /**
     * calculateEstimatedYield() turns a few of the values that were passed from the prev destinations
     * into type float to be used to calculate the MINIMUM estimated yield
     */
    private void calculateEstimatedYield() {

        float areaOfSampleSize = 16;
        float avgWeightOfMangoes = 200;
        float firstOpAsFloat = Float.parseFloat(firstOperator);
        float secondOpAsFloat = Float.parseFloat(secondOperator);
        float countAsFloat = Float.parseFloat(count);

        //Source: https://www.producemarketguide.com/produce/apples
        double applePricePerPound = 1.69;

        float totalArea = firstOpAsFloat * secondOpAsFloat;
        estimatedYield = (countAsFloat * avgWeightOfMangoes) * (totalArea / areaOfSampleSize);
        estimatedPrice = estimatedYield * applePricePerPound * 0.00220462 * conversionFactor;

    }

    /**
     * setEstimatedYieldDisplay() just sets the TextView with id="estimated_yield_display"
     * to the value in calculateEstimatedYield()
     */
    private void setEstimatedYieldDisplay() {
        TextView estimatedYieldDisplay = mView.findViewById(R.id.estimated_yield_display);
        String temp = estimatedYield + " grams";
        estimatedYieldDisplay.setText(temp);

        TextView value1Display = mView.findViewById(R.id.value1);
        displayPrice = String.format("%.2f", estimatedPrice);
        value1Display.setText(displayPrice);
    }
    /** Opens settings.txt to read the user's currency preference. Defaults to USD if it does not exist. */
    private String readFromCache(Context context) {

        String ret = "USD";

        try {
            InputStream inputStream = context.openFileInput("settings.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private void saveYield(){
        //Here is where we will save all data that was passed
        // we will make the table user which will pass the data onto the setter functions in the User.java


        User user = new User();

        user.setNameOfYield(nameOfYield);
        user.setFirstOperator(firstOperator);
        user.setSecondOperator(secondOperator);
        user.setSpinnerChoice(spinnerChoice);
        user.setCount(count);
        user.setYield(estimatedYield + "");
        user.setValue(currencySymbol + displayPrice);

        //This function will show a message on the bottom of the screen
        //that will say the "Added Yield Successfully" when the data is saved

        MainActivity.myAppDatabase.myDao().addUser(user);
        Toast.makeText(getActivity(),"Added Yield Successfully", Toast.LENGTH_SHORT).show();

        //Once the user is done saving the yield, the user will be navigated to the home screen
        Navigation.findNavController(mView).navigate(R.id.homeFragment);

    }
}
