package com.example.yield;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.room.Room;

import com.example.yield.R;
import com.google.android.material.navigation.NavigationView;


/**
 * The MainActivity class is the NavHost for the Navigation Graph in
 * res/navigation/nav_graph.xml
 *
 * The Navigation Drawer and Toolbar get created in onCreate().
 * The NavigationView also gets setup with our NavController
 *
 * Navigation from the Navigation Drawer gets handled in onNavigationItemSelected()
 * which is why NavigationView.OnNavigationItemSelectedListener NEEDS to be implemented
 */
public class MainActivity extends AppCompatActivity implements
                                NavigationView.OnNavigationItemSelectedListener {

    private NavigationView navigationView;
    private NavController navController;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;


    //for add yield
    public static MyAppDatabase myAppDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //id: navigation_view created in activity_main.xml
        navigationView = findViewById(R.id.navigation_view);

        //id: drawer_layout created in activity_main.xml
        drawerLayout = findViewById(R.id.drawer_layout);

        //id: nav_host_fragment created in activity_main.xml
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        //id: toolbar created in activity_main.xml
        toolbar = findViewById(R.id.toolbar);

        //
        myAppDatabase = Room.databaseBuilder(getApplicationContext(),MyAppDatabase.class,"userdb").allowMainThreadQueries().build();
        //This sends the info into the database

        /*
         * setSupportActionBar() sets a Toolbar to act as the ActionBar for this Activity window.
         *
         * In order to get rid of original action bar and use our own to work with the Navigation
         * Drawer, in res/values/styles.xml we must add:
         *      <item name="windowActionBar">false</item>
         *      <item name="windowNoTitle">true</item>
         */
        setSupportActionBar(toolbar);

        /*
         * Sets up the NavigationView for use with the NavController.
         * The NavController manages the app navigation within the NavHost.
         * The NavHost in this case is our MainActivity.java
         */
        NavigationUI.setupWithNavController(navigationView, navController);

        /*
         * Sets up the ActionBar to work with a NavController
         *
         * The Action Bar that gets set is the one we passed in setSupportActionBar()
         *
         * The start destination of your navigation graph is considered the only top level destination.
         * On the start destination of your navigation graph, the ActionBar will show the drawer icon if
         * the given DrawerLayout is non null.
         * On all other destinations, the ActionBar will show the Up button.
         */
        NavigationUI.setupActionBarWithNavController(this, navController, drawerLayout);


    }

    /************************************************************************************
     * The onSupportNavigateUp() method is called whenever the user chooses to navigate Up within
     * your application's activity hierarchy from the action bar.

     * navigateUp() handles the Up button by delegating its behavior to the given NavController.
     * Will return true if the NavController was able to navigate up.
     */
    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(navController, drawerLayout);
    }


    /************************************************************************************
    * If the user has the Navigation Drawer open and presses the back button on the phone,
    * the Navigation Drawer will close.

    * If the user has the Navigation Drawer closed and presses the back button on the phone,
    * onBackPressed() will be called and the default implementation simply finishes the current activity,
    * but you can override this to do whatever you want.
    */
    @Override
    public void onBackPressed() {
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    /************************************************************************************
     * onNavigationItemSelected is called when an item in the navigation menu is selected.
     *
     * implements NavigationView.OnNavigationItemSelectedListener in class declaration
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {

        int id = menuItem.getItemId();

        /*
         * The android:id for each menu item matches the same android:id
         * for each Fragment it corresponds to
         *
         * Once a case is triggered, a new Fragment is created and sent
         * to show() which is a helper function
         *
         * Once the switch statements ends, the Navigation Drawer gets closed
         */

        switch (id) {

            case R.id.addYieldFragment:
                Fragment addYieldFragment = new AddYieldFragment();
                show(addYieldFragment);
                break;
            case R.id.viewYieldFragment:
                Fragment viewYieldFragment = new ViewYieldFragment();
                show(viewYieldFragment);
                break;
            case R.id.settingsFragment:
                Fragment settingsFragment = new SettingsFragment();
                show(settingsFragment);
                break;
        }

        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    /********
     * A FragmentManager is created to handle the transaction and
     * start a series of edit operations on the Fragments associated with
     * this FragmentManager.
     *
     * The Fragment that was chosen will then replace the current screen
     */
    private void show(Fragment fragment) {

        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        FragmentManager fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction().replace(R.id.nav_host_fragment, fragment).commit();

        drawerLayout.closeDrawer(GravityCompat.START);

    }
}
