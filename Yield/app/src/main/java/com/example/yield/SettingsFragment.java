package com.example.yield;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * Class for Settings Fragment that's in the Navigation Graph
 * HomeFragment -> SettingsFragment -> ...
 */
public class SettingsFragment extends Fragment {

    private String currencyChoice;

    private View mView;
    private Context mContext;

    // Required empty public constructor
    public SettingsFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    /**
     * onViewCreated() gets called AFTER onCreateView() has returned.
     *
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //context and view used for live access the spinner object as the user interacts with it.
        mContext = getContext();
        mView = getView();

        // setSpinner() gets called to create the Spinner (dropdown menu) that will hold
        //  the string values for our different currencies
        setSpinner();



        /*
         * view.findViewById() returns the "Tutorials" button in fragment_settings.xml
         *
         * If the user clicks on that button, createNavigateOnClickListener() is called
         * and the user will be taken to the Tutorials Fragment in the
         * navigation graph
         */
        view.findViewById(R.id.settings_tutorials).
                setOnClickListener(Navigation.createNavigateOnClickListener(R.id.tutorialsFragment, null));

        // Defines behavior for what happens when the user clicks the save button.
        view.findViewById(R.id.save_settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextDestination();
            }
        });
    }

    /**
     * setSpinner() gets called in onViewCreated to set up the Spinner (dropdown menu)
     * that holds the values for our currencies
     */
    public void setSpinner() {

        //id: currency_spinner is created in settings_yield.xml
        Spinner spinner = (Spinner) mView.findViewById(R.id.currency_spinner);


        // Create an ArrayAdapter using the string array, crops_array, which is found in res/values/strings.xml
        // and gives a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(mContext,
                R.array.currency_array, android.R.layout.simple_spinner_item);


        // Specifies the layout to use when the list of choices appear
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Applies the adapter to the spinner
        spinner.setAdapter(adapter);

        // Updates a variable with the user's spinner choice. Defaults to USD.
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                currencyChoice = (String) parent.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                currencyChoice = "USD";
            }
        });



    }


    /**
     * nextDestination() gets called in onViewCreated(), specifically in the onClick() for our button
     * with android:id = save_settings.
     */
    private void nextDestination() {

        // Displays a message informing the user that their settings have been saved.
            Toast.makeText(getContext(), "Saving "+currencyChoice+" as your currency preference.", Toast.LENGTH_SHORT).show();

            getTempFile(mContext, "settings.txt");
            writeSettingsToCache(mContext);
    }

    /** Creates a file in cache memory. */
    private File getTempFile(Context context, String url) {
        File file = null;
        try {
            String fileName = Uri.parse(url).getLastPathSegment();
            file = File.createTempFile(fileName, null, context.getCacheDir());
        } catch (IOException e) {
            Toast.makeText(getContext(), "There was an error while saving your data.", Toast.LENGTH_SHORT).show();
        }
        return file;
    }

    /** Writes the contents of currencyChoice to the settings file in cache memory. */
    private void writeSettingsToCache(Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("settings.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(currencyChoice);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Toast.makeText(getContext(), "There was an error while saving your data.", Toast.LENGTH_SHORT).show();
        }
    }


}
