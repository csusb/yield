package com.example.yield;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

//Here we need to create a table for the data to be stored
//reference Room database for more information
@Entity(tableName = "users")
public class User
{
    //create entities in tables
    //one of the variables must have @PrimaryKey (The Key that could be used to find the
    // other data that is associated with the primary key )
    //The string that have ColumnInfo which displays what is in the " " as the column name in the database table.


    //This String will be a primary key that will auto-generate a count from 1 so each saved data has an assigned ID
    @PrimaryKey(autoGenerate = true)
    private int id;

    //The String will be the name that the user will enter for their yield on the addyieldFragment
    @ColumnInfo(name = "Name_Yield")
    private String nameOfYield;

    //This String is on the AddYieldFragment .java and .xml , this is the first input space for size of yield
    @ColumnInfo(name = "first_operator")
    private String firstOperator;


    //This String is on the AddYieldFragment .java and .xml , this is the second input space for size of yield
    @ColumnInfo(name = "second_operator")
    private String secondOperator;


    //This String is on the AddYieldFragment .java and .xml , this is the spinner button: Choose Crop input
    @ColumnInfo(name = "spinner_choice")
    private String spinnerChoice;


    //This String is on the ManualEntryFragment .java and xml , this get the "Count how much of the desired crop you see in Y * Y area "
    @ColumnInfo(name = "user_count")
    private String count;

    @ColumnInfo(name = "estimated_yield")
    private String yield;

    @ColumnInfo(name = "estimated_value")
    private String value;


    //to access these variable need to have getter and setter methods
    //each String declared must have a getter and setter method


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameOfYield() {
        return nameOfYield;
    }

    public void setNameOfYield(String nameOfYield) {
        this.nameOfYield = nameOfYield;
    }

    public String getFirstOperator() {
        return firstOperator;
    }

    public void setFirstOperator(String firstOperator) {
        this.firstOperator = firstOperator;
    }

    public String getSecondOperator() {
        return secondOperator;
    }

    public void setSecondOperator(String secondOperator) {
        this.secondOperator = secondOperator;
    }

    public String getSpinnerChoice() {
        return spinnerChoice;
    }

    public void setSpinnerChoice(String spinnerChoice) {
        this.spinnerChoice = spinnerChoice;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getYield() {return yield; }

    public void setYield(String yield) { this.yield = yield; }

    public String getValue() {return value; }

    public void setValue(String value) { this.value = value; }

}
