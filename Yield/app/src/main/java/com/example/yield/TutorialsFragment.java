package com.example.yield;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.yield.R;


/**
 * Class for Tutorials Fragment that's in the Navigation Graph
 * HomeFragment -> SettingsFragment -> TutorialsFragment -> ...
 */
public class TutorialsFragment extends Fragment {


    public TutorialsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tutorials, container, false);
    }

    @Override
    public void onViewCreated (@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);
        view.findViewById(R.id.next).setOnClickListener(Navigation.createNavigateOnClickListener(R.id.tutorial1));
    }

}
